# Dashboard

Este proyecto fue generado para la materia de Seminario de Sistemas de Información para la Toma de Decisiones.


# Autores
            M.A.T.I. Juan Luis Uriel Rodríguez Peralta  
            M.I.S. Adolfo Manuel de la Cruz García 
            M.S.C. Irahan Otoniel José Guzmán 
            M.C.C. José Alfredo Falconis Ballinas



# Demo 

https://seminariotomadedecisiones.netlify.app



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.



## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
