import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: number;
}
const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Alabama', weight: 3285448, symbol: 659400 },
  { position: 2, name: 'Alaska', weight: 641221, symbol: 825625 },
  { position: 3, name: 'Arizona', weight: 6347847, symbol: 8021020 },
  { position: 4, name: 'Arkansas', weight: 2190235, symbol: 2817700 },
  { position: 5, name: 'California', weight: 41584951, symbol: 48577915 },
  { position: 6, name: 'Colorado', weight: 6046139, symbol: 6963445 },
  { position: 7, name: 'Connecticut', weight: 4320717, symbol: 4678675},
  { position: 8, name: 'Delaware', weight: 1010503, symbol: 1265495 },
  { position: 9, name: 'District of Columbia', weight: 844373, symbol: 1053315 },
  { position: 10, name: 'Florida', weight: 20477457, symbol: 24590285 },
  { position: 11, name: 'Georgia', weight: 8100255, symbol: 11051315},
  { position: 12, name: 'Hawaii', weight: 1661877, symbol: 1925920 },
  { position: 13, name: 'Idaho', weight: 1302916, symbol: 1656570 },
  { position: 14, name: 'Illinois', weight: 12612678, symbol: 14528485 },
  { position: 15, name: 'Indiana', weight: 5486097, symbol: 6615380 },
  { position: 16, name: 'Iowa', weight: 2993465, symbol: 3425225},
  { position: 17, name: 'Kansas', weight:2442071, symbol: 3057805 },
  { position: 18, name: 'Kentucky', weight: 3910764, symbol: 4411665 },
  { position: 19, name: 'Louisiana', weight: 3278754, symbol: 4012140 },
  { position: 20, name: 'Marshall Islands', weight:  32470, symbol: 51300 },
  { position: 21, name: 'Maryland', weight:  6659768, symbol: 8354700 },
  { position: 22, name: 'Massachusetts', weight:  8680160, symbol: 9628260 },
  { position: 23, name: 'Michigan', weight:  9277147, symbol: 11523660 },
  { position: 24, name: 'Minnesota', weight:  5754966, symbol: 6479520 },
  { position: 25, name: 'Mississippi', weight:  1861535, symbol: 2643905 },
  { position: 26, name: 'Missouri', weight:  5009706, symbol: 6064045 },
  { position: 27, name: 'Montana', weight:  921549, symbol: 1102395 },
  { position: 28, name: 'Nebraska', weight:  1785309, symbol: 2069710 },
  { position: 29, name: 'Nevada', weight:  2692825, symbol: 3023840 },
  { position: 30, name: 'New Hampshire', weight:  1500484, symbol: 1806250 },
  { position: 31, name: 'New Jersey', weight:  9893949, symbol: 11412635},
  { position: 32, name: 'New Mexico', weight:  2296027, symbol: 2339245},
  { position: 33, name: 'New York State', weight:  21268599, symbol: 23615385},
  { position: 34, name: 'North Carolina', weight:  9893949, symbol: 11594650},
  { position: 35, name: 'North Dakota', weight:  635388, symbol: 709060},
  { position: 36, name: 'Ohio', weight:  10509262, symbol: 12291785},
  { position: 37, name: 'Oklahoma', weight:  3197986, symbol: 3996710},
  { position: 38, name: 'Oregon', weight:  4442610, symbol: 5604375},
  { position: 39, name: 'Pennsylvania', weight:  13631788, symbol: 15473895},
  { position: 40, name: 'Rhode Island', weight:  1251082, symbol: 1496785},
  { position: 41, name: 'South Carolina', weight:  4039136, symbol: 5238505},
  { position: 42, name: 'South Dakota', weight:  805837, symbol: 952065},
  { position: 43, name: 'Tennessee', weight:  5163651, symbol: 7255050},
  { position: 44, name: 'Texas', weight:  25049611, symbol: 31438315},
  { position: 45, name: 'Utah', weight:  9893949, symbol: 11412635},
  { position: 46, name: 'Vermont', weight:  2759165, symbol: 3196670},
  { position: 47, name: 'Virgin Islands', weight:  839933, symbol: 912630},
  { position: 48, name: 'Virginia', weight:  9040430, symbol: 10225595},
  { position: 49, name: 'Washington', weight:  8359815, symbol: 9293435},
  { position: 50, name: 'West Virginia', weight:  1391862, symbol: 1878315},
  { position: 51, name: 'Wisconsin', weight:  5785928, symbol: 6066895},
  { position: 52, name: 'Wyoming', weight: 413169, symbol: 513015}
];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  bigChart = [];
  bigChart2 = [];
  bigChart3 = [];
  bigChart4 = [];
  bigChart5 = [];
  bigChart6 = [];
  bigChart7 = [];
  bigChart8 = [];

  cards = [];
  pieChart = [];

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.bigChart = this.dashboardService.bigChart();
    this.bigChart2 = this.dashboardService.bigChart2();
    this.bigChart3 = this.dashboardService.bigChart3();
    this.bigChart4 = this.dashboardService.bigChart4();
    this.bigChart5 = this.dashboardService.bigChart5();
    this.bigChart6 = this.dashboardService.bigChart6();
    this.bigChart7 = this.dashboardService.bigChart7();
    this.bigChart8 = this.dashboardService.bigChart8();

    this.cards = this.dashboardService.cards();
    this.pieChart = this.dashboardService.pieChart();

    this.dataSource.paginator = this.paginator;
  }

}
